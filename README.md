# php-extended/php-geojson-interface
A specification library to implement geojson objects features

![coverage](https://gitlab.com/php-extended/php-geojson-interface/badges/master/pipeline.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar install php-extended/php-geojson-interface ^8`


## Basic Usage

This library is an interface only library.

For a concrete implementation see [php-geojson-object](https://gitlab.com/php-extended/php-geojson-object).


## License

MIT (See [license file](LICENSE)).
