<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\GeoJson;

/**
 * GeoJsonObjectVisitorInterface interface file.
 * 
 * This interface defines a visitor for all GeoJSON objects, including
 * geometry objects.
 * 
 * @author Anastaszor
 */
interface GeoJsonObjectVisitorInterface extends GeoJsonGeometryVisitorInterface
{
	
	/**
	 * Visits a feature with the given visitor.
	 * 
	 * @param GeoJsonFeatureInterface $feature
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitFeature(GeoJsonFeatureInterface $feature);
	
	/**
	 * Visits a feature collection with the given visitor.
	 * 
	 * @param GeoJsonFeatureCollectionInterface $featureCollection
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitFeatureCollection(GeoJsonFeatureCollectionInterface $featureCollection);
	
}
