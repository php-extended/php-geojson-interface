<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\GeoJson;

use Stringable;

/**
 * GeoJsonObjectInterface interface file.
 * 
 * This interface specifies a generic geojson object. This interface is not
 * meant to be implemented directly, but as a base interface for the real
 * object features.
 * 
 * @author Anastaszor
 * @abstract
 */
interface GeoJsonObjectInterface extends Stringable
{
	
	/**
	 * Gets the type of this geojson object.
	 * 
	 * @return string
	 */
	public function getType() : string;
	
	/**
	 * Gets the bounding box of this object.
	 * 
	 * @return ?GeoJsonBoundingBoxInterface
	 */
	public function getBoundingBox() : ?GeoJsonBoundingBoxInterface;
	
	/**
	 * Visits this object with the given visitor object.
	 * 
	 * @param GeoJsonObjectVisitorInterface $visitor
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(GeoJsonObjectVisitorInterface $visitor);
	
}
