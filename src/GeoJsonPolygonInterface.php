<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\GeoJson;

/**
 * GeoJsonPolygonInterface interface file.
 * 
 * This interface specifies a polygon GeoJSON object.
 * 
 * @author Anastaszor
 */
interface GeoJsonPolygonInterface extends GeoJsonGeometryInterface
{
	
	/**
	 * Gets the coordinates of the polygon.
	 * 
	 * @return GeoJsonPolygonCoordinateInterface
	 */
	public function getCoordinates() : GeoJsonPolygonCoordinateInterface;
	
}
