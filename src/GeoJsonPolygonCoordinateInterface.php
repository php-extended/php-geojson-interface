<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\GeoJson;

use Iterator;

/**
 * GeoJsonPolygonCoordinateInterface interface file.
 * 
 * This interface specifies a polygonal shape.
 * 
 * @author Anastaszor
 */
interface GeoJsonPolygonCoordinateInterface extends GeoJsonCoordinateInterface
{
	
	/**
	 * Gets the exterior line string.
	 * 
	 * @return GeoJsonLineStringCoordinateInterface
	 */
	public function getExteriorLine() : GeoJsonLineStringCoordinateInterface;
	
	/**
	 * Gets the interior line strings.
	 * 
	 * @return Iterator<GeoJsonLineStringCoordinateInterface>
	 */
	public function getInteriorLines() : Iterator;
	
	/**
	 * Gets all the line strings.
	 * 
	 * @return Iterator<GeoJsonLineStringCoordinateInterface>
	 */
	public function getLines() : Iterator;
	
}
