<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\GeoJson;

use Stringable;

/**
 * GeoJsonCoordinateInterface interface file.
 * 
 * This interface specifies the commons between all coordinate record objects.
 * 
 * @author Anastaszor
 */
interface GeoJsonCoordinateInterface extends Stringable
{
	
	/**
	 * Visits this object with the given visitor object.
	 * 
	 * @param GeoJsonCoordinateVisitorInterface $visitor
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(GeoJsonCoordinateVisitorInterface $visitor);
	
}
