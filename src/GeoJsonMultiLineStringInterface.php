<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\GeoJson;

/**
 * GeoJsonMultiLineStringInterface interface file.
 * 
 * This interface specifies a multi line string GeoJson object.
 * 
 * @author Anastaszor
 */
interface GeoJsonMultiLineStringInterface extends GeoJsonGeometryInterface
{
	
	/**
	 * Gets the coordinates of this multi line string.
	 * 
	 * @return GeoJsonMultiLineStringCoordinateInterface
	 */
	public function getCoordinates() : GeoJsonMultiLineStringCoordinateInterface;
	
}
