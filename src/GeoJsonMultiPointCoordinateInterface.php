<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\GeoJson;

use Iterator;

/**
 * GeoJsonMultiPointCoordinateInterface interface file.
 * 
 * This interface specifies a multi point shape.
 * 
 * @author Anastaszor
 */
interface GeoJsonMultiPointCoordinateInterface extends GeoJsonCoordinateInterface
{
	
	/**
	 * Gets the coordinates in order for this multi point ring.
	 * 
	 * @return Iterator<GeoJsonPointCoordinateInterface>
	 */
	public function getPoints() : Iterator;
	
}
