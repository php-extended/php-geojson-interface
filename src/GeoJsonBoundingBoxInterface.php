<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\GeoJson;

use Stringable;

/**
 * GeoJsonBoundingBoxInterface interface file.
 * 
 * This interface describes a bounding box for any GeoJson object.
 * 
 * @author Anastaszor
 */
interface GeoJsonBoundingBoxInterface extends Stringable
{
	
	/**
	 * Gets the west plane of the box.
	 * 
	 * @return float
	 */
	public function getWest() : float;
	
	/**
	 * Gets the north plane of the box.
	 * 
	 * @return float
	 */
	public function getNorth() : float;
	
	/**
	 * Gets the depth of the box, if provided.
	 * 
	 * @return ?float
	 */
	public function getDepth() : ?float;
	
	/**
	 * Gets the east plane of the box.
	 * 
	 * @return float
	 */
	public function getEast() : float;
	
	/**
	 * Gets the south plane of the box.
	 * 
	 * @return float
	 */
	public function getSouth() : float;
	
	/**
	 * Gets the height of the box, if provided.
	 * 
	 * @return ?float
	 */
	public function getHeight() : ?float;
	
}
