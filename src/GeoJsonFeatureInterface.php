<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\GeoJson;

/**
 * GeoJsonFeatureInterface interface file.
 * 
 * This interface specifies a feature GeoJSON object.
 * 
 * @author Anastaszor
 */
interface GeoJsonFeatureInterface extends GeoJsonObjectInterface
{
	
	/**
	 * Gets the identifier of this feature object.
	 * 
	 * @return ?string
	 */
	public function getIdentifier() : ?string;
	
	/**
	 * Gets the geometry associated to this feature.
	 * 
	 * @return GeoJsonGeometryInterface
	 */
	public function getGeometry() : GeoJsonGeometryInterface;
	
	/**
	 * Gets the properties of the feature.
	 * 
	 * @return array<string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>>
	 */
	public function getProperties() : array;
	
}
