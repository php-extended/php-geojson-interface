<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\GeoJson;

use Iterator;

/**
 * GeoJsonGeometryCollectionInterface interface file.
 * 
 * This interface specifies a geometry collection GeoJSON object.
 * 
 * @author Anastaszor
 */
interface GeoJsonGeometryCollectionInterface extends GeoJsonGeometryInterface
{
	
	/**
	 * Gets the children geometries objects.
	 * 
	 * @return Iterator<GeoJsonGeometryInterface>
	 */
	public function getGeometries() : Iterator;
	
}
