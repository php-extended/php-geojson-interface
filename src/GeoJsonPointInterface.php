<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\GeoJson;

/**
 * GeoJsonPointInterface interface file.
 * 
 * This interface specifies a point GeoJSON object.
 * 
 * @author Anastaszor
 */
interface GeoJsonPointInterface extends GeoJsonGeometryInterface
{
	
	/**
	 * Gets the coordinates of the position.
	 * 
	 * @return GeoJsonPointCoordinateInterface
	 */
	public function getCoordinates() : GeoJsonPointCoordinateInterface;
	
}
