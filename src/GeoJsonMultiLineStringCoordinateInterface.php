<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\GeoJson;

use Iterator;

/**
 * GeoJsonMultiLineStringCoordinateInterface interface file.
 * 
 * This interface specifies a multi line string shape.
 * 
 * @author Anastaszor
 */
interface GeoJsonMultiLineStringCoordinateInterface extends GeoJsonCoordinateInterface
{
	
	/**
	 * Gets the line strings of this multi line string.
	 * 
	 * @return Iterator<GeoJsonLineStringCoordinateInterface>
	 */
	public function getLineStrings() : Iterator;
	
}
