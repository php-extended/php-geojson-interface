<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\GeoJson;

use Stringable;

/**
 * GeoJsonGeometryVisitorInterface interface file.
 * 
 * This interface specifies a visitor for geometry types.
 * 
 * @author Anastaszor
 */
interface GeoJsonGeometryVisitorInterface extends Stringable
{
	
	/**
	 * Visits a geometry collection with the given visitor.
	 * 
	 * @param GeoJsonGeometryCollectionInterface $collection
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitGeometryCollection(GeoJsonGeometryCollectionInterface $collection);
	
	/**
	 * Visits a line with the given visitor.
	 * 
	 * @param GeoJsonLineStringInterface $lineString
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitLineString(GeoJsonLineStringInterface $lineString);
	
	/**
	 * Visits a multi line with the given visitor.
	 * 
	 * @param GeoJsonMultiLineStringInterface $multiLineString
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitMultiLineString(GeoJsonMultiLineStringInterface $multiLineString);
	
	/**
	 * Visits a point with the given visitor.
	 * 
	 * @param GeoJsonPointInterface $point
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitPoint(GeoJsonPointInterface $point);
	
	/**
	 * Visits a multi point with the given visitor.
	 * 
	 * @param GeoJsonMultiPointInterface $multiPoint
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitMultiPoint(GeoJsonMultiPointInterface $multiPoint);
	
	/**
	 * Visits a polygon with the given visitor.
	 * 
	 * @param GeoJsonPolygonInterface $polygon
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitPolygon(GeoJsonPolygonInterface $polygon);
	
	/**
	 * Visits a multi polygon with the given visitor.
	 * 
	 * @param GeoJsonMultiPolygonInterface $multiPolygon
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitMultiPolygon(GeoJsonMultiPolygonInterface $multiPolygon);
	
}
