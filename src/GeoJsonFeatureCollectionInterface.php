<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\GeoJson;

use Iterator;

/**
 * GeoJsonFeatureCollectionInterface interface file.
 * 
 * This interface specifies a feature collection GeoJSON object.
 * 
 * @author Anastaszor
 */
interface GeoJsonFeatureCollectionInterface extends GeoJsonObjectInterface
{
	
	/**
	 * Gets the features in this feature collection.
	 * 
	 * @return Iterator<GeoJsonFeatureInterface>
	 */
	public function getFeatures() : Iterator;
	
}
