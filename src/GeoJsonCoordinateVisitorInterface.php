<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\GeoJson;

use Stringable;

/**
 * GeoJsonCoordinateVisitorInterface interface file.
 * 
 * This interface defines a visitor for coordinate structures.
 * 
 * @author Anastaszor
 */
interface GeoJsonCoordinateVisitorInterface extends Stringable
{
	
	/**
	 * Visits a line coordinate with the given visitor.
	 * 
	 * @param GeoJsonLineStringCoordinateInterface $coordinate
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitLineStringCoordinate(GeoJsonLineStringCoordinateInterface $coordinate);
	
	/**
	 * Visits a multi line coordinate with the given visitor.
	 * null.
	 * 
	 * @param GeoJsonMultiLineStringCoordinateInterface $coordinate
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitMultiLineStringCoordinate(GeoJsonMultiLineStringCoordinateInterface $coordinate);
	
	/**
	 * Visits a point coordinate with the given visitor.
	 * 
	 * @param GeoJsonPointCoordinateInterface $coordinate
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitPointCoordinate(GeoJsonPointCoordinateInterface $coordinate);
	
	/**
	 * Visits a multi point coordinate with the given visitor.
	 * 
	 * @param GeoJsonMultiPointCoordinateInterface $coordinate
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitMultiPointCoordinate(GeoJsonMultiPointCoordinateInterface $coordinate);
	
	/**
	 * Visits a polygon coordinate with the given visitor.
	 * 
	 * @param GeoJsonPolygonCoordinateInterface $coordinate
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitPolygonCoordinate(GeoJsonPolygonCoordinateInterface $coordinate);
	
	/**
	 * Visits a multi polygon coordinate with the given visitor.
	 * 
	 * @param GeoJsonMultiPolygonCoordinateInterface $coordinate
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitMultiPolygonCoordinate(GeoJsonMultiPolygonCoordinateInterface $coordinate);
	
}
