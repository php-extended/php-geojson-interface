<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\GeoJson;

/**
 * GeoJsonPointCoordinateInterface interface file.
 * 
 * This interface specifies a coordinate point on the globe.
 * 
 * @author Anastaszor
 */
interface GeoJsonPointCoordinateInterface extends GeoJsonCoordinateInterface
{
	
	/**
	 * Gets the latitude value of the coordinate.
	 * 
	 * @return float
	 */
	public function getLatitude() : float;
	
	/**
	 * Gets the longitude value of the coordinate.
	 * 
	 * @return float
	 */
	public function getLongitude() : float;
	
	/**
	 * Gets the altitude value of the coordinate.
	 * 
	 * @return ?float
	 */
	public function getAltitude() : ?float;
	
}
