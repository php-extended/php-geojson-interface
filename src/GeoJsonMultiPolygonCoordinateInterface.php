<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\GeoJson;

use Iterator;

/**
 * GeoJsonMultiPolygonCoordinateInterface interface file.
 * 
 * This interface specifies a multi polygon shape.
 * 
 * @author Anastaszor
 */
interface GeoJsonMultiPolygonCoordinateInterface extends GeoJsonCoordinateInterface
{
	
	/**
	 * Gets the polygons for this muti polygon.
	 * 
	 * @return Iterator<GeoJsonPolygonCoordinateInterface>
	 */
	public function getPolygons() : Iterator;
	
}
