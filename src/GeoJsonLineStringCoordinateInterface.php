<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\GeoJson;

use Iterator;

/**
 * GeoJsonLineStringCoordinateInterface interface file.
 * 
 * This interface specifies a line string shape.
 * 
 * @author Anastaszor
 */
interface GeoJsonLineStringCoordinateInterface extends GeoJsonCoordinateInterface
{
	
	/**
	 * Gets the first point of the line string.
	 * 
	 * @return GeoJsonPointCoordinateInterface
	 */
	public function getFirstPoint() : GeoJsonPointCoordinateInterface;
	
	/**
	 * Gets the last point of the line string.
	 * 
	 * @return GeoJsonPointCoordinateInterface
	 */
	public function getLastPoint() : GeoJsonPointCoordinateInterface;
	
	/**
	 * Gets the coordinates in order for this line string.
	 * 
	 * @return Iterator<GeoJsonPointCoordinateInterface>
	 */
	public function getPoints() : Iterator;
	
}
