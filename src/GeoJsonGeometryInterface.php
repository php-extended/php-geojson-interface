<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\GeoJson;

/**
 * GeoJsonGeometryInterface interface file.
 * 
 * This interface specifies a geometry GeoJSON object.
 * 
 * @author Anastaszor
 */
interface GeoJsonGeometryInterface extends GeoJsonObjectInterface
{
	
	/**
	 * Visits this object with the given visitor object.
	 * 
	 * @param GeoJsonGeometryVisitorInterface $visitor
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedByGeometry(GeoJsonGeometryVisitorInterface $visitor);
	
}
