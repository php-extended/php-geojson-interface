<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-geojson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\GeoJson;

/**
 * GeoJsonMultiPointInterface interface file.
 * 
 * This interface specifies a multi point GeoJSON object.
 * 
 * @author Anastaszor
 */
interface GeoJsonMultiPointInterface extends GeoJsonGeometryInterface
{
	
	/**
	 * Gets the coordinates of the multi point.
	 * 
	 * @return GeoJsonMultiPointCoordinateInterface
	 */
	public function getCoordinates() : GeoJsonMultiPointCoordinateInterface;
	
}
